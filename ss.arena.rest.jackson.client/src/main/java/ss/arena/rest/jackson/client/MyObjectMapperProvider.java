package ss.arena.rest.jackson.client;

import java.time.LocalDate;

import javax.ws.rs.ext.ContextResolver;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class MyObjectMapperProvider implements ContextResolver<ObjectMapper> {

    private ObjectMapper objectMapper;
    
    public MyObjectMapperProvider() {
        objectMapper = new ObjectMapper()
                .setVisibility(PropertyAccessor.ALL, Visibility.NONE)
                .setVisibility(PropertyAccessor.FIELD, Visibility.ANY)
                .setVisibility(PropertyAccessor.CREATOR, Visibility.ANY); // use fields instead of getters/setters
        
        SimpleModule module = new SimpleModule();
        module.addSerializer(LocalDate.class, new LocalDateSerializer());
        module.addDeserializer(LocalDate.class, new LocalDateDeserializer());
        objectMapper.registerModule(module);
    }
    
    @Override
    public ObjectMapper getContext(Class<?> arg0) {
        return objectMapper;
    }
}
