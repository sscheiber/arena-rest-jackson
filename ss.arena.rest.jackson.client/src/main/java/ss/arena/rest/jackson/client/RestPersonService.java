package ss.arena.rest.jackson.client;

import java.net.URI;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson.JacksonFeature;

import ss.arena.rest.jackson.model.Person;

public class RestPersonService {
    private static final String DEFAULT_HOST = "http://localhost/";
    private static final int DEFAULT_PORT = 8088;
    
    private final WebTarget target;
    
    public RestPersonService() {
        this(DEFAULT_HOST, DEFAULT_PORT);
    }
    
    public RestPersonService(String host, int port) {
        URI baseUri = UriBuilder.fromUri(host).port(port).path("rest").path("persons").build();
        ClientConfig config = new ClientConfig(JacksonFeature.class, MyObjectMapperProvider.class);
        Client client = ClientBuilder.newClient(config);
        target = client.target(baseUri);
    }
    
    public List<Person> getPersons() {
        GenericType<List<Person>> personListType = new GenericType<List<Person>>() {};
        return target.request(MediaType.APPLICATION_JSON).get(personListType);
    }
    
    public List<Person> getPersonsByLastname(String lastname) {
        GenericType<List<Person>> personListType = new GenericType<List<Person>>() {};
        return target.path(lastname).request(MediaType.APPLICATION_JSON).get(personListType);
    }
    
    public void savePerson(Person person) {
        Response response = target.request().post(Entity.entity(person, MediaType.APPLICATION_JSON));
        if (response.getStatus() != Response.Status.CREATED.getStatusCode()) {
            throw new RuntimeException(response.readEntity(String.class));
        }
    }
    
    public void deletePerson(Person person) {
        target.path(String.valueOf(person.getId())).request().delete();
    }
}
