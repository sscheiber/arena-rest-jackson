package ss.arena.rest.jackson.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;
import java.util.List;

import org.junit.Test;

import ss.arena.rest.jackson.model.Person;

public class RestPersonServiceIT {
    private RestPersonService personService = new RestPersonService();
    
    @Test
    public void testRestPersonService() {
        List<Person> persons = personService.getPersons();
        assertTrue(persons.isEmpty());
        personService.savePerson(new Person("John", "Smith", LocalDate.of(1975, 7, 7)));
        personService.savePerson(new Person("Brian", "Jackson"));
        personService.savePerson(new Person("Adam", "Smith"));
        persons = personService.getPersons();
        assertEquals(3, persons.size());
        List<Person> smiths = personService.getPersonsByLastname("Smith");
        assertEquals(2, smiths.size());
        for (Person p: persons) {
            personService.deletePerson(p);
        }
        persons = personService.getPersons();
        assertTrue(persons.isEmpty());
    }
}
