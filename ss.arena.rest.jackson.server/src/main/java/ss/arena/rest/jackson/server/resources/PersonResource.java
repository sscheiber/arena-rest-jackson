package ss.arena.rest.jackson.server.resources;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import ss.arena.rest.jackson.model.Person;

@Path("persons")
public class PersonResource {
    private static int generator = 0;
    private static final List<Person> PERSONS = new ArrayList<>();

    @Context
    private UriInfo uriInfo;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Person> getPersons() {
        return PERSONS;
    }

    @GET
    @Path("{lastName}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Person> getPersonsByLastName(@PathParam("lastName") String lastName) {
        return PERSONS.stream().filter(p -> lastName.equals(p.getLastName())).collect(Collectors.toList());
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addPerson(Person person) {
        try {
            Field idField = person.getClass().getDeclaredField("id");
            idField.setAccessible(true);
            idField.set(person, ++generator);
            idField.setAccessible(false);
            PERSONS.add(person);
            URI uri = uriInfo.getAbsolutePathBuilder().path(String.valueOf(person.getId())).build();
            return Response.created(uri).build();
        } catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @DELETE
    @Path("{id}")
    public void deletePerson(@PathParam("id") String personId) {
        int id = Integer.parseInt(personId);
        PERSONS.removeIf(p -> p.getId() == id);
    }
}
