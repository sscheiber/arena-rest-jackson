package ss.arena.rest.jackson.server;

import java.util.Set;

import javax.ws.rs.core.Application;

import org.glassfish.jersey.jackson.JacksonFeature;

import jersey.repackaged.com.google.common.collect.Sets;
import ss.arena.rest.jackson.server.resources.PersonResource;

public class MyApplication extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        return Sets.newHashSet(PersonResource.class, JacksonFeature.class, MyObjectMapperProvider.class);
    }
}
